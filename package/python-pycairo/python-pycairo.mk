################################################################################
#
# python-pycairo
#
################################################################################

PYTHON_PYCAIRO_VERSION = 1.16.1
PYTHON_PYCAIRO_SOURCE = pycairo-$(PYTHON_PYCAIRO_VERSION).tar.gz
PYTHON_PYCAIRO_SITE = https://pypi.python.org/packages/1f/8d/992c2c80c0fd56417029e886786cc1b40e6a55aba1c39f1418fad6f7c9aa
PYTHON_PYCAIRO_SETUP_TYPE = distutils
PYTHON_PYCAIRO_LICENSE_FILES = COPYING
PYTHON_PYCAIRO_DEPENDENCIES = cairo
PYTHON_PYCAIRO_INSTALL_STAGING = YES

$(eval $(python-package))
