################################################################################
#
# prelink-cross
#
################################################################################

PRELINK_CROSS_VERSION=05aeafd053e56356ec8c62f4bb8f7b95bae192f3
PRELINK_CROSS_SITE=http://git.yoctoproject.org/cgit/cgit.cgi/prelink-cross/snapshot
PRELINK_CROSS_SOURCE=prelink-cross-${PRELINK_CROSS_VERSION}.tar.bz2
HOST_PRELINK_CROSS_AUTORECONF = YES
HOST_PRELINK_CROSS_DEPENDENCIES = host-elfutils host-binutils
PRELINK_CROSS_INSTALL_HOST=YES

define PRELINK_CROSS_INSTALL_CONF
	$(INSTALL) -m 0755 -D package/prelink-cross/prelink.conf ${STAGING_DIR}/etc/prelink.conf
endef
PRELINK_CMD="${HOST_DIR}/usr/sbin/prelink --verbose --config-file /etc/prelink.conf --cache-file /etc/prelink.cache --root=${TARGET_DIR} --all"
HOST_PRELINK_CROSS_POST_INSTALL_HOOKS += PRELINK_CROSS_INSTALL_CONF
# The point of prelink-cross is to run it on the host. We deliberately do not
# provide a way to compile it for the target, only the host.
$(eval $(host-autotools-package))
