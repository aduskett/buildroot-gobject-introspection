################################################################################
#
# python-pygobject
#
################################################################################

PYTHON_PYGOBJECT_VERSION_MAJOR = 3.26
PYTHON_PYGOBJECT_VERSION = $(PYTHON_PYGOBJECT_VERSION_MAJOR).1
PYTHON_PYGOBJECT_SITE = http://ftp.gnome.org/pub/GNOME/sources/pygobject/$(PYTHON_PYGOBJECT_VERSION_MAJOR)
PYTHON_PYGOBJECT_SOURCE = pygobject-$(PYTHON_PYGOBJECT_VERSION).tar.xz
PYTHON_PYGOBJECT_LICENSE_FILES = COPYING docs/images/LICENSE
PYTHON_PYGOBJECT_DEPENDENCIES = gobject-introspection autoconf-archive

# for 0001-add-PYTHON_INCLUDES-override.patch
PYTHON_PYGOBJECT_AUTORECONF = YES
PYTHON_PYGOBJECT_CONF_OPTS += --disable-cairo

ifeq ($(BR2_PACKAGE_PYTHON),y)
PYTHON_PYGOBJECT_DEPENDENCIES += python host-python

PYTHON_PYGOBJECT_CONF_ENV = \
	PYTHON=$(HOST_DIR)/bin/python2 \
	PYTHON_INCLUDES="`$(STAGING_DIR)/usr/bin/python2-config --includes`"
else
PYTHON_PYGOBJECT_DEPENDENCIES += python3 host-python3

PYTHON_PYGOBJECT_CONF_ENV = \
	PYTHON=$(HOST_DIR)/bin/python3 \
	PYTHON_INCLUDES="`$(STAGING_DIR)/usr/bin/python3-config --includes`"
endif

PYTHON_PYGOBJECT_CONF_ENV += \
	PYTHON_SO=".cpython-36m-$(BR2_ARCH)-linux-gnueabihf.so"

ifeq ($(BR2_PACKAGE_LIBFFI),y)
PYTHON_PYGOBJECT_CONF_OPTS += --with-ffi
PYTHON_PYGOBJECT_DEPENDENCIES += libffi
else
PYTHON_PYGOBJECT_CONF_OPTS += --without-ffi
endif

$(eval $(autotools-package))
